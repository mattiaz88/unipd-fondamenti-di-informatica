/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Sort;
import stack.*;
/**
 *
 * @author mattia
 */
public class Stack_MergeSort {
    /*
        Nonostante l'apparente maggiore complessità, le prestazioni asintotiche rimangono O(n log n)
    
        Tutte le operazioni sulle pile sono O(1) come gli accessi ad un elemento di un array, l'analisi
        delle prestazioni dell'algoritmo fornisce ancora 
        
        T(n) = 2T(n/2) + O(n) -> O(n log n)
    */
    
    
   /*
        La pila deve contenere oggetti Comparable
    
        - non conosciamo la dimensione della pila
        - una soluzione semplice consiste nell'estrarre gli elementi della pila ed inserirli 
          alternativamente id 2 semi-pile
        
    */
    
    
    
    private Stack stack;
    
    public Stack_MergeSort(){

        this.stack = new GrowingArrayStack();
        for(int i = 1;i<=20;i++){
            stack.push(i);
        }
        mergeSort(stack);
        
        while(!stack.isEmpty()){
            try{
                System.out.println(stack.pop());
            }
            catch(Exception e ){
                //error
                System.out.println(e.getMessage());
            }
        }
    }
    
    private void mergeSort(Stack s){
        
        //caso base
        if(s==null || s.isEmpty()){
            return;
        }
        //case base -> dimensione 1
        Object temp = s.pop();
        if(s.isEmpty()){
            s.push(temp);
            return;
        }
        
        //dividiamo circa a metà
        boolean flag = true;
        Stack stackLeft = new GrowingArrayStack();
        Stack stackRight = new GrowingArrayStack();
        // reinserisco temp perché precedentemente tolto
        stackLeft.push(temp);
        while(!s.isEmpty()){
            
            if(flag){
                stackRight.push(s.pop());
            }
            else{
                stackLeft.push(s.pop());
            }
            flag = !flag;
        }
        
        mergeSort(stackLeft);
        mergeSort(stackRight);
        
        //fusione: s è rimasta vuota!
        merge(s,stackLeft,stackRight);
    }
    
    private void merge(Stack s,Stack stackLeft,Stack stackRight){
        
        //serve per invertire il contenuto alla fine
        Stack temp = new GrowingArrayStack();
        while(!stackLeft.isEmpty() && !stackRight.isEmpty()){
            
            try{
                Comparable x = (Comparable) stackLeft.top();
                Comparable y = (Comparable) stackRight.top();

                if(x.compareTo(y) < 0){
                    temp.push(stackLeft.pop());
                }
                else{
                    temp.push(stackRight.pop());
                }
            }
            catch(ClassCastException e){
                //gestisco errore
                return;
            }
            
        }
        
        
        while(!stackLeft.isEmpty()){
            temp.push(stackLeft.pop());
        }
        while(!stackRight.isEmpty()){
            temp.push(stackRight.pop());
        }
        
        //inverte il contenuto perchè altrimenti in cima alla pila ci sarebbe l'elemento maggiore
        while(!temp.isEmpty()){
            s.push(temp.pop());
        }   
        
    }
        
        
}
