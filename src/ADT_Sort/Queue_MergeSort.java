/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Sort;
import queue.*;

/**
 *
 * @author mattia
 */
public class Queue_MergeSort {
    /*
        Nonostante l'apparente maggiore complessità, le prestazioni asintotiche rimangono O(n log n)
    
        Tutte le operazioni sulle pile sono O(1) come gli accessi ad un elemento di un array, l'analisi
        delle prestazioni dell'algoritmo fornisce ancora 
        
        T(n) = 2T(n/2) + O(n) -> O(n log n)
    */
    
    
   /*
        La pila deve contenere oggetti Comparable
    
        - non conosciamo la dimensione della pila
        - una soluzione semplice consiste nell'estrarre gli elementi della pila ed inserirli 
          alternativamente id 2 semi-pile
        
    */
    
    private Queue queue;
    
    public Queue_MergeSort(){

        this.queue = new GrowingFixedCircularArrayQueue();
        for(int i = 0;i<=20;i++){
            queue.enqueue(i);
        }
        mergeSort(queue);
        
        while(!queue.isEmpty()){
            try{
                System.out.println(queue.dequeue());
            }
            catch(Exception e ){
                //error
                System.out.println(e.getMessage());
            }
        }
    }
    
    
    private void mergeSort(Queue q){
        
       //casi base
       if(q == null || q.isEmpty()){
           return;
       }
       Object temp = q.dequeue();
       if(q.isEmpty()){
           q.enqueue(temp);
           return;
       }
       //////////////
       
       boolean flag = true;
       Queue leftQueue = new GrowingFixedCircularArrayQueue();
       Queue rightQueue =  new GrowingFixedCircularArrayQueue();
       leftQueue.enqueue(temp);
       while(!q.isEmpty()){
           //System.out.println(q.getFront());
           if(flag){
               rightQueue.enqueue(q.dequeue());
           }else{
               leftQueue.enqueue(q.dequeue());
           }
           flag=!flag;
       }
       
       mergeSort(leftQueue);
       mergeSort(rightQueue);
       
       merge(q,leftQueue,rightQueue);
       
    }
    
    private void merge(Queue q,Queue leftQueue,Queue rightQueue){
        
        while(!leftQueue.isEmpty() && !rightQueue.isEmpty()){
            
            Comparable x = (Comparable) leftQueue.getFront();
            Comparable y = (Comparable) rightQueue.getFront();
            
            if(x.compareTo(y)<0){
                q.enqueue(leftQueue.dequeue());
            }
            else{
                q.enqueue(rightQueue.dequeue());
            }
            
        }
        
        while(!leftQueue.isEmpty()){
           q.enqueue(leftQueue.dequeue());
        }
        while(!rightQueue.isEmpty()){
           q.enqueue(rightQueue.dequeue());
        }
        
    }
        
        
}
