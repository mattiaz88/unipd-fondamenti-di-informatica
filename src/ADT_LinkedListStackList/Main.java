/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_LinkedListStackList;

import stack.*;
import linkedList.*;

/**
 *
 * @author Mattia
 */
public class Main{
    
    /*
        STRUTTURE DATI COMPLESSE
    
        Pila con iteratore implementata con una catena
        
    */
   
   public static void main() {
       
       /*
            Esiste il comportamento complesso definito da un'interfaccia
       
             LinkedListStackList t = new LinkedListStackList();
             Stack s = t;
             List list = t;
       
             I 2 assegnamenti ad interfaccia sono corretti perché la classe LinkedListStackList realizza entrambe
             le interfacce, quindi suoi esemplari possono essere visti come una qualsiasi delle 2 interfacce
            
       */
       LinkedListStackList t = new LinkedListStackList();
       
       Stack s = t; // conversione riferimento
       s.push("X");
       s.push("Y");
       s.push("Z");
       
       List list = t; // conversione riferimento
       ListIterator iter = list.getIterator();
       while(iter.hasNext()){
           System.out.println(iter.next());
       }
       s.makeEmpty(); //svuoto la pila
       
       iter = list.getIterator(); // gli iteratori non possono tornare indietro, quini ne creo un'altro
       while(iter.hasNext()){
           System.out.println(iter.next());
       }
       //non viene visualizzato niente perché la lista è vuota
   }
   
}
