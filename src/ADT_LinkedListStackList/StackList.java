/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_LinkedListStackList;

import stack.*;
import linkedList.*;

/**
 *
 * @author Mattia
 */

public interface StackList extends Stack,List {
    //una interfaccia può avere più di una super interfaccia a differenza delle classi
 
    /*
        La cosa migliore sarebbe la definizione astratta del comportamento dato dall'unione dei 2 comportamenti, 
        Stack e List: successivamente , la classe LinkedLIstStackList si definosce come relaizzazione di tale
        comportamento complesso.
    */
      
}
