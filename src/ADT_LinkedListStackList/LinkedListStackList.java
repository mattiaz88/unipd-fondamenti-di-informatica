/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_LinkedListStackList;

import dictionary.*;
import stack.*;
import linkedList.*;

/**
 *
 * @author Mattia
 */
public class LinkedListStackList implements StackList{
    
    /*
        STRUTTURE DATI COMPLESSE
    
        Pila con iteratore implementata con una catena
    
        In alcune situazioni può essere comodo avere a disposizione  una struttura dati che realizzi 
        contemporaneamente il comportamento di più tipi di dati astatti:
            - ad esempio, una pial che metta a disposizione anche un iteratore per ispezionare tutti i suoi elementi,
              come una lista
    
    
    RIASSUMENDO:
    
    Una classe JAVA può:
        - estendere una sola altra classe, oppure implicitamente estendere Object
        - realizzare più interfacce
    
    Se una classe realizza più di una interfaccia:
        - deve definire tutti i metodi di tutte le interfacce
        - ha quindi un comportamente che è l'unione dei comporamenti definiti dalle diverse interfacce
        
    */
   
   private LinkedList list = new LinkedList();
   public void makeEmpty(){ //Container
       list.makeEmpty();
   }
   
   public boolean isEmpty(){ // Container
       return list.isEmpty();
   }
   
   public void push(Object obj){ // Stack
       list.addFirst(obj);
   }
   public Object pop(){ // Stack
       return list.removeFirst(); 
   } 
   public Object top(){ // Stack
       return list.getFirst();
   }
   public ListIterator getIterator(){ // List
       return list.getIterator();
   }
   
}
