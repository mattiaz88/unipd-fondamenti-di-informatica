/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Bank;


/**
 *
 * @author Mattia
 */
public class BankAccount {
    
    private double balance;
   
    public BankAccount(){
       balance = 0;
    }
    public BankAccount(double amount){
       balance = amount;
    }
    
    public void deposit(double amount){
        balance += amount;
    }
    
    public void withdraw(double amount){
        balance -= amount;
    }
    
    public double getBalance(){
        return balance;
    }

    /*
        CONVERSIONE FRA RIFERIMENTI
        
        Un riferimento ad un oggetto di una classe derivata può essere assegnato ad una variabile oggetto
        del tipo di una sua superclasse: NON C'E' UNA CONVERSIONE EFFEETTIVA perché c'è un unico oggetto
        in gioco.
    
        SavingsAccount sAcct = new SavingsAccount(10);
        BankAccount bacct = sAcct;
        sAcct.deposit(500);  -> OK!
    
        Il riferimento bacct non può accedere alle proprietà di SavingsAccount.
        In fase di esecuzione bAcct.deposit(500) va a controllare l'indirizzo di memoria dell'oggetto
        SavingsAccount e chiama il metodo deposit.
    
    
        bacct.addInterest(); -> NO! 
        
        Il compilatore in questo caso va a cercare il metodo in BankAccount e non lo trova!
    
    
        BankAccount other = new BankAccount(1000);
        SavingsAccount sAcct = new SavingsAccount(10);
        other.transferTo(sAcct,500);
    
        La conversione tra riferimento a sottoclasse e riferimento a superclasse può avvenire anche
        implicitamente (come tra int e double)
    
    
    
        SavingsAccount bAcct = new SavingsAccount(10);
        BankAccount bAcct = sAcct;
        SavingsAccount sAcct2 = (SavingsAccount) bAcct;
        
        La conversione di un riferimento a una superclasse in un riferimento a sottoclasse ha senso
        soltanto se, per le specifiche dell'algortimo, siamo sicuri che il riferimento a superclasse punta
        in realtà ad un oggetto della sottoclasse: richiede un CAST esplicito.
        
        Se l'algoritmo richiede un cast di questo tipo, è meglio cautelarsi:
        -   si può inserire il cast in un bocco try/catch che gestisca l'eccezione ClassCastException 
            (la cui gestione non è obbligatoria)
        -   si può usare l'operatore "instanceof"
    
        SavingsAccount bAcct = new SavingsAccount(10);
        BankAccount bAcct = sAcct;
    
        if(bAcct instaceof SavingsAccount)
            SavingsAccount sAcct2 = (SavingsAccount) bAcct;
            
        L'operatore instanceof è un iperatore booleano che restituisce true se e solo se la variabileOggetto   
        contiene un riferimento ad un oggetto che è un esemplare della NomeClasse o di una sua sottoclasse:
        in tal caso l'assegnamento di variabileOggetto ad una variabile di tipo NomeClasse NON lancia 
        l'eccezione ClassCastException.
        Il risultato non dipende dal tipo dichiarato per la variabileOggetto, ma dal tipo dell'oggetto a cui 
        la variabile si riferisce effettivamente al momento dell'esecuzione.
        
        
    */
    public void transferTo(BankAccount other,double amount){
        withdraw(amount);
        other.deposit(amount);
    }
    
} 
