/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Bank;


/**
 *
 * @author Mattia
 */
public class SavingsAccount extends BankAccount{
    
    /*
        Ereditarietà
        
        E' uno dei proncipi basilari della programmazione orientata agli oggetti, insieme
        all'incapsulamento e al poliformismo.
        L'eridtarietà è un paradigma che consente il riutilizzo del codice: si usa quando si deve 
        realizzare una classe ed è già disponibile un'altra classe che raprresenta un concetto più generale.
    
    */
    
    private double interestRate;
    private final static double FEE = 2.58; //euro
    
    /*
        SavingsAccount è una classe (sottoclasse) derivata da BankAccount (superclasse): 
        -   ne erdita tutte le caratteristiche
        -   specifichiamo soltanto le peculiarità
    
        In Java, ogni classe che non deriva da nessun'altra deriva implicitamente dalla superclasse
        universale del linguaggio, chi si chiama Object.
        L'ereditarietà avviene anche su più livelli, quindi SavingsAccoutn eredita anche le proprietà da
        Object.
        
        I termini superclasse e sottoclasse derivano dalla teoria degli insiemi, dove una sottoclasse 
        costituisce un sottoinsieme di ogni superclasse.
    
        Quando si definisce una sottoclasse, per quanto riguardano i suoi metodi può succedere che:
    
        -   viene definito un metodo che nella superclasse non esisteva
        -   un metodo della superclasse viene eriditato
        -   un metodo della superclasse viene sovrascritto nella sottoclasse
       
        
    */
    public SavingsAccount(double rate){
       interestRate = rate;
    }
    
    /*
        Il riferimento super è gestito automaticamente deal compilatore per accedere agli elementi 
        ereditati dalla superclasse. 
        Super è privato e non accessibile dall'esterno ma solamente dall'interno dell'oggetto.
        L'invocazione esplicita di super(..), se presente, deve essere il primo enunciato del costruttore.
        Se non è indicato esplicitamente, viene invocato implicitamente super() senza parametri.
        Il costruttore predefinito (senza argomenti) esiste implicitamente SOLTANTO se nella classe non
        vengono definiti dei costruttori
    */
    public SavingsAccount(double rate,double initialAmount){
       super(initialAmount);
       interestRate = rate;
    }
    
    public void addInterest(double amount){
        deposit(getBalance() * interestRate / 100);
    }
    
    
    /*  
        OVERRIDE
    
        La possibilità di sovrascrivere (override) un metodo della superclasse, modificandone il comportamento
        quando è usato per la sottoclasse, è una delle caratteristiche più potenti di OOP.
        Per poter sovrascrivere un metodo bisogna definire nella sottoclasse un metodo con la stessa firma
        di quello definitio nella superclasse: tale metodo prevale su quello della superclasse quado viene 
        invocato con un oggetto della sottoclasse.
    */
    public void deposit(double amount){
        withdraw(FEE);
        //invoca deposit della superclasse
        super.deposit(amount);
        
        /*
            deposit(amount) -> non funzionerebbe perché il metodo diventerebbe un metodo ricorsivo
            con una ricorsione infinita!!
        */
    }

    
} 
