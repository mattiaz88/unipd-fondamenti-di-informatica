/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stack;

/**
 *
 * @author Mattia
 */
public class EmptyStackException extends RuntimeException {

   /*
        Un metodo può lanciare eccezioni a controllo non obbligatorio:
        - eccezioni che derivano da RuntimeException
    
        Se, invece, un metodo vuole lanciare eccezioni a controllo obbligatorio:
        - l'eccezione deve essere derivata da Exception
        - nella firma del metodo deve essere dichiarato l'elenco delle eccezioni a controllo
          obbligatorio lanciate nel metodo separate da virgole
          - si usa la clausola throws
          
          void method(...) throws Exception1,Exception2
    
          throws non lancia eccezioni in questo caso, ma dichiara che il metodo può lanciarle
          - è iuna segnalazione al compilatore, che così obbliga  all'uso di un blocco try/catch nei metodi
            che invocano tale metodo
    
        
        FILOSOFIA
        Si dovrebbero lanciare eccezioni:
        - A controllo non obbligatorio
            - quando si verifica una situazione di errore a causa di un errore del programmatore
        - A controllo obbligatorio
            - quando si verifica una situazione di errore non dipendente dal programmatore
                - interazioni con l'utente
                - interazioni con la rete
                - interazioni con il sistema operativo (filsystem,...)
    */ 
    
}
