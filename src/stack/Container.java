/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stack;

/**
 *
 * @author Mattia
 */

/*
    Tipi di dati astratti

    Una struttura dati (data structure) è un modo sistrmatico di organizzare i dati in un contenitore
    e di controllarne le modalità di accesso: in  Java si definisce una struttura dati  con una classe.

    Un tipo di dati astratti (ADT), Abstract Data Type, è una rappresentazione astratta di una struttura
    dati, un modello che specifica:
    
    - il tipo di dati memorizzati
    - le operazioni che si possono eseguire sui dati: il tipo di informazioni necessarie per eseguire
      le operazioni

    
    In Java si definisce un tipo di dato astratto con una interfaccia.
    Un' interfaccia descrive un comportamento che sarà assunto da una classe che realizza l'interfaccia: 
    è proprio quello che serve per definire un ADT.

    Un ADT definisce cosa si può fare con una struttura dati che realizza l'interfaccia: la classe che
    rappresenta concretamente la struttura dati definisce come vengono eseguite le operazioni.

    Il pacchetto java.util della libreria standard contiene molte definzioni di ADT come interfacce e le 
    loro realizzazioni come classi.

    Un tipo di dati astratto mette in generale a disposizione metodi per svolgere le seguenti azioni
    (a volte solo alcune):

    - inserimento di un elemento
    - rimozione di un elemento
    - ispezione degli elementi contenuti nella struttura: ricerca di un elemento all'interno della struttura

    I diversi ADT si differenziano per le modalità di funzionamento di queste 3 azioni.
*/

// Contenitore generico
public interface Container {

    boolean isEmpty();
    void makeEmpty();
    
}
