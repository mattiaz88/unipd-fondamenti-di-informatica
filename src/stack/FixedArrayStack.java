/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stack;

/**
 *
 * @author Mattia
 */
public class FixedArrayStack implements Stack {

    protected Object[] v;
    protected int vSize;
    
    public FixedArrayStack(){
        v = new Object[100];
        
        makeEmpty();
    }
    
    public void makeEmpty(){
        vSize = 0;
    }
    
    public boolean isEmpty(){
        return (vSize==0);
    }
    ///////////////////////////////////
   
    public void push(Object obj){
        if(vSize == v.length){
            throw new FullStackException();
        }
        v[vSize++]=obj;
    };
    
    public Object top(){
        
        if(isEmpty()){
            throw new EmptyStackException();
        }
        return v[vSize-1];
    }
    
    public Object pop(){
        Object obj = top();
        vSize--;
        return v[vSize]; 
    }
    
    
}
