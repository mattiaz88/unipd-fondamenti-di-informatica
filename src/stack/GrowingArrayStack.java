/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stack;

/**
 *
 * @author Mattia
 */
public class GrowingArrayStack extends FixedArrayStack {

  
    public void push(Object obj){
        if(vSize == v.length){
            v = resize(v,2*vSize);
        }
        super.push(obj);
    };

    
    // resize the underlying array holding the elements
    private Object[] resize(Object[] v,int capacity) {
        assert capacity >= vSize;
        Object[] temp = new Object[capacity];
        for (int i = 0; i < vSize; i++) {
            temp[i] = v[i];
        }
        return temp;
    }
    
}
