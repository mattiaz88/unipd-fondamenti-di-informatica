/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package stack;

/**
 *
 * @author Mattia
 */

/*
    In una pila (stack) gli oggetti possono essere inseriti ed estratti secondo un comportamento definito
    LIFO (Last In, Last Out):
    
    - l'ultimo daoggetto inserito è  il primo ad essere estratto  
    - il nome èè stato scelto in analogia con una pila di piatti

    L'unico oggetto che può essere ispezionato p quello che si trova in cima alla pila.
    Esistono  molti possibili utilizzi di una struttura dati con quesro comportamento: la JVM usa una pila 
    per memorizzare l'elenco dei metodi in attesa durante l'esecuzione in un dato istante.


    I metodi che caratterizzano una pila sono:
    
        - push per inserire un oggetto in cima alla pila
        - pop per eliminare l'oggetto che si trova in cima alla pila
        - top per ispezionare l'oggetto che si trova in cima alla pila, senza estrarlo
    
        
        Ogni ADT tipo "contenitore" ha i metodi (comportamento generico):
        
        - isEmpty per sapere se il contenitore è vuoto
        - makeEmpty per svuotare il contenitore
    
        Progettare un dato astratto => definire delle interfacce
                                    => definire dei metodi
*/
public interface Stack extends Container {

    /*
        Anche le interfacce, come le classi, possono essere "estese".
        Una interfaccia eredita tutti i suoi metodi della sua super-interfaccia.
        Per realizzare un'interfaccia estesa occorre definire anche i suoi metodi della sua super-interfaccia.
    */
    
    
    void push(Object obj);
    Object pop();// pop restituisce un riferimento ad un oggetto che sta in cima alla pila
    Object top();// senza parametri perchè può eliminare o ispezionare l'elemento che sta in cima alla pila
    
    
    
}
