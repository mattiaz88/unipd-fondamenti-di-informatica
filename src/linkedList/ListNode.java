/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;

/**
 *
 * @author mattia
 */
public class ListNode{
   
        private Object element;
        private ListNode next;
        
        public ListNode(Object element,ListNode n){
            this.element = element;
            this.next = n;
        }
        
        public ListNode(){
            element = null;
            next = null;
        }

        public Object getElement(){
            return element;
        }
        public ListNode getNext(){
            return next;
        }
        public void setElement(Object e){
            element = e;
        }
        public void setNext(ListNode n){
            next = n;
        }
        
        
    
    
}
