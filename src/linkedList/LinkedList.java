/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;

/**
 *
 * @author mattia
 */
public class LinkedList implements List { // implements Container
   
    
    /*
        La catena o lista concatenata (linked list) non èun nuovo ADT, ma è una struttura dati alternativa  
        all'array per la realizzazione di ADT.
        Una catena è un insieme ordinato di nodi dove ogni nodo è un oggetto che contiene:
        - un riferimento ad un elemento (il dato)
        - un riferimento al nodo successivo nella catena (next)
    
        Per agire sulla catena è sufficiente memorizzare il riferimento al suo primo nodo
        - è comodo avere un riferimento all'ultimo nodo
        - è comodo avere un  primo ndo senza dati, chiamato header
        Il campo next dell'ultimo nodo contiene null.
    
        Per capire il funzionamento della catena, è necessario avere ben chiarala rappresentazione della 
        catena vuota:
        - contiene solo il nodo header, che ha null in entrambi i suoi campi
        - head e tail puntano entrambi a tale header
        
        Per accedere in sequenza a tutti i nodi della catena si parte dal riferimento inizio e si seguono 
        i riferimenti contenuti nel campo next di ciascun nodo:
        - non è possibile scorrere la lista in senso inverso
        - la scansione termina quando si trova il nodo con il valore null nel campo next
    
        AUTO-RIFERIMENTO
        La  classe definisce e usa riferimenti ad oggetti del tipo che si sta definendo: si usa molto spesso
        quando si rappresentano "strutture a definizione ricorsiva" come la catena.
    
        I metodi utili per una catena sono:
        - addFirst per inserire un oggetto all'inizio della catena
        - addLast per inserire un oggetto alla fine della catena
        - removeFirst per eliminare il primo oggetto della catena
        - removeLast per eliminare l'ultimo oggetto della catena
    
        Spesso si aggiungono anche i metodi:
        - getFirst per esaminare il primo oggetto
        - getLast per esaminare l'ultimo oggetto
    
        Si ossevi che non vengono mai restituti nè ricevuti riferimenti ai nodi, ma sempre ai dati contenuti 
        nei nodi.
        Si noti che, non essendo la catena un ADT, non viene definita un'interfaccia:
        - la catena non è un ADT perchè nella sua definizione abbiamo esplicitamente indicato COME la struttura
          dati deve essere ralizzata, e non il suo comportamento.
    
    
        PRESTAZIONI
        Tutte le operazioni sulla catena sono O(1) tranne removeLast che è O(n):
        - si potrebbe pensare  di tenere il riferimento anche al penultimo nodo, ma per aggiornare tale 
          riferimento sarebbe comunque necessario un tempo O(n).
        
        Se si usa la catena con il solo riferimento head, anche add Last diventa O(n):
        - per questo è utile usare il riferimento tail, che migliora le prestazioni di addLast senza peggiorare 
          le altre e non richiede molto spazio di memoria.
    
        Non esiste il problema di "catena piena":
        - non bisogna mai "ridimensionare" la catena
        - la JVM lancia l'eccezione OutOfMemoryError se viene esaurita la memoria disponibile.
        
        Non c'è spazio di memoria sprecato (come un array "riempito solo in parte"):
        - un nodo occupa però più spazio di una cella di array, almeno il doppio (contiene 2 riferimenti
          anzichè uno).
    
    
        CLASSI INTERNE
        La classe ListNode non viene usata al di fuori della catena stessa:
        - la catena non restituisce mai riferimenti a ListNode
        - la catena non riceve mai riferimenti a ListNode
    
        Per il principio dell'incapsulamento (information hiding) sarebbe preferibile che questa classe e 
        i suoi detaggli non fossero visbili all'esterno della catena.
        In questo modo una modifica della struttura interna della catena e/o di ListNode non avrebbe 
        ripercussioni sul codice scritto da chi usa la catena.
    
        Il linguaggio JAVA consente di definire classi all'interno di un'altra classe: tali classi si
        chiamano classi interne (inner classes).
        Se una classe interna viene definita private essae è accessibile soltanto all'interno della classe
        in cui è definita: dall'esterno non è nemmeno possibile creare oggetti di tale classe interna
        
        public class LinkedList...
        {
            ...
             
            private class ListNode
            {
                ...
            }
        }
    
    */
    
        private ListNode head,tail;
        
        public LinkedList(){
            makeEmpty();
        }
        
        public void makeEmpty(){
            head = tail = new ListNode();
        }
        
        public boolean isEmpty(){
            return (head == tail);
        }
        
        public Object getFirst(){ //operazione O(1)
            if(isEmpty()){
                throw new EmptyLinkedListException();
            }
            return head.getNext().getElement();
        }
        public Object getLast(){ //operazione O(1)
            if(isEmpty()){
                throw new EmptyLinkedListException();
            }
            return tail.getElement();
        }
        
        public void addFirst(Object element){ //operazione O(1)
            
            // inserisco il dato nell'header attuale
            head.setElement(element);
            //creo un nuovo nodo con 2 riferimenti null
            ListNode node = new ListNode();
            //collego il nuovo nodo dell'header attuale
            node.setNext(head);
            //il nuovo nodo diventa l'header della catena
            head = node;
            // tail non viene modificato
        }
        
        public Object removeFirst(){ //operazione O(1)
            
            //delega getFirst il controllodi lista vuota
            Object obj = getFirst();
            //aggiorno l'header
            head = head.getNext();
            head.setElement(null);
            
            return obj;
        }
        
        public void addLast(Object obj){ //operazione O(1)
            
            ListNode node = new ListNode(obj,null);
            //node.setElement(obj);
            //node.setNext(null);
            tail.setNext(node);
            tail = tail.getNext();
            
            
        }
        
        //purtroppo questa operazione richiede un tempo di esecuzione pari a O(n)
        public Object removeLast(){ 
            
            Object obj = getLast();
            //bisogna cercare il penultimo nodo partendo dall'inizio e andando avanti finché non si arriva 
            //alla fine della catena
            
            ListNode temp=head;
            while(temp.getNext()!=tail){
                temp = temp.getNext();
            }
            //e a questo punto temp si riferisce al prenultimo nodo
            tail = temp;
            tail.setNext(null);
            return obj;
        }
        
        /*
            Per contare gli elementi presenti in una catena è necessario scorrere tutta la catena
            
            Ossrviamo che per eseguire algoritmi sulla catena è necessario aggiungere metodi all'interno della 
            classe LinkedList, chè è l'unica ad avere accesso ai nodi della catena:
            - ad esempio un metodo che verifichi la presenza di un particolare oggetto della catena (algoritmo di
              ricerca)
            Questo limita molto l'utilizzo della catena come struttura definita una volta per tutte.
            Vogliamo che la catena fornisca uno strumento per accedere direttamente a tutti i suoi elementi.
            L'idea più semplice è quella di fornire un metodo getHead
            
            public class LinkedList ...
                public ListNode getHead(){
                    return head;
                }
            }
        
            ma questo viola completamente l'incapsulamento
            perché diventa impossibile modificare direattamente lo stato interno della catena, anche in modo da non
            farla più funzionare correttamente.
        */
        public int getSize(){
            ListNode temp = head.getNext();
            int size = 0;
            while(temp.getNext()!=null){
                size++;
                temp = temp.getNext();
            }
            // osservare che size è zero se la catena è vuota
            return size;
        }
        
        /*
            Per un corretto funzionamento dell'iteratore occorre concedere a tale oggetto il pieno
            accesso alla catena
        */
        public LinkedListIterator getIterator()
        {
            return new LinkedListIterator(head);
        }
        
        
        /*
            Se vengono creati più iteratori che agiscono sulla stessa lista, ciascuno di essi mantiene
            il proprio stato, cioè memorizza la propria posizione nella lista.
            Ciascun iteratore può muoversi nella lista indipendentemente dagli altri:
            - occorre però usare qualche cautela quando si usano gli iteratori per modificare la lista
            
        */
        private class LinkedListIterator implements ListIterator
        {
            // nodo che precede la posizione attuale (non è mai null, perché c'è l'header) 
            private ListNode current;
            
            public LinkedListIterator(ListNode node){
                current = head;
            }
            
            public boolean hasNext(){
                if(current.getNext()!=null){
                    return true;
                }
                return false;
            }
            public Object next(){
                if(hasNext()){
                    throw new IllegalStateException();
                }
                current = current.getNext();
                return current.getElement();
            }
            public void remove(){
                //TODO
            }
            
            public void add(){
                //TODO
            }
        }
        
        
}