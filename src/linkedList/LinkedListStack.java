/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;
import stack.*;
/**
 *
 * @author mattia
 */
public class LinkedListStack implements Stack {
   
    private LinkedList list = new LinkedList();
    /*
      
    
    */
        public void makeEmpty(){
            list.makeEmpty();
        }
        
        public boolean isEmpty(){
            return list.isEmpty();
        }
        
        public void push(Object element){ //operazione O(1)
            
            list.addFirst(element);
        }
        
        public Object top(){ //operazione O(1)
            return list.getFirst();
        }
      
        
        public Object pop(){ //operazione O(1)
            
            return list.removeFirst();
        }
        
     
        
}