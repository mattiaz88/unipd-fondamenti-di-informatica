/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;
import queue.*;
/**
 *
 * @author mattia
 */
public class LinkedListQueue implements Queue {
   
    private LinkedList list = new LinkedList();
    /*
      
        Le prestazioni della pila o coda realizzati con le catene sono identiche che con gli array
    */
        public void makeEmpty(){
            list.makeEmpty();
        }
        
        public boolean isEmpty(){
            return list.isEmpty();
        }
        
        public void enqueue(Object element){ //operazione O(1)
            
            list.addLast(element);
        }
        
        public Object getFront(){ //operazione O(1)
            return list.getFirst();
        }
      
        
        public Object dequeue(){ //operazione O(1)
            
            return list.removeFirst();
        }
        
     
        
}