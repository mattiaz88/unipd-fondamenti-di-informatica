/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;

/**
 *
 * @author Mattia
 */
public interface ListIterator {

    /*
        La soluzione del problema è quella di fornire all'utilizzatore della atena uno strumento con cui 
        interagire con la catena per scandire i suoi nodi. Tale oggetto si chiamata iteratore e ne definiamo 
        prima di tutto il suo comportamento astratto:
        - un iteratore raprresenta in astratto il cncetto di posizione all'interno di una catena
        - un iteratore si trova senore DOPO un nodo e PRIMA del nodo successivo (che può non esistere se 
          l'iteratore si trova dopo l'ultimo nodo)
        - quando viene creato, l'iteratore si trovo dopo il nodo header
        - la posizione è rappresentata concretamente da un riferimento ad un nodo (il nodo precedente alla 
          posizione dell'iteratore)
    
    
        Funzionamento del costruttore:
        quando viene costruito, l'iteratore si trova nella prima posizione, cioè DOPO il nodo header.
        Se l'iteratore si trova alla fine della catena, lancia IllegalStateException, altrienti restituisce
        l'oggetto che si trova nel nodo posto DOPO la posizione attuale e sposta l'iteratore di una posizione 
        in avanti lungo la catena.
    */
    Object next();
    
    /*
       verifica se è disponibile invocare next() senza che venga lanciata un'eccezione
    */
    boolean hasNext();
    
    /*
        inserire l'oggetto X in un nuovo nodo che si aggiunge alla catena PRIMA della posizione attuale, senza
        modificare la posizione dell'iteratore
    */
    void add();
    
    /*
        elimina l'ultimo nodo esaminato da next() oppure l'ultimo nodo inserito con add(), senza modificare
        la posizione dell'iteratore; non può essere invocato 2 volte cnsecutive (lancia IllegalStaeException).
    */
    void remove();
}
