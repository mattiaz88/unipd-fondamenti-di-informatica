/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package linkedList;

/**
 *
 * @author Mattia
 */
public interface List extends Container{

    /*
        Con l'iteratore completo, possiamo osservare che l'interfaccia ListIterator, oltre a consentire la
        manipolazione di una catena, definisce anche il comportamento astratto di un contenitore in cui:
        - i dati sono disposti in sequenza
        - nuovi dati possono essere inseriti in ogni punto della sequenza
        - dati possono essere rimossi da qualsiasi punto della sequenza
        
        Un contenitore avente un tale comportamento può essere molto utile, per cui si definisce un tipo
        di dati astratti, detto LISTA (da non confondere con lista concatenata -> CATENA !).
    */
    
    /*
        Ci sono diversi tipi quindi di contenitori per dati in sequenza, rappresentati dagli ADT:
        - pila
        - coda
        - lista con iteratore
        Per realizzare gli ADT si usano generalmente diverse strutture dati, tra cui:
        - array
        - catene
    */
    ListIterator getIterator();
    
}
