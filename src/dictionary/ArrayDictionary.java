/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dictionary;



/**
 *
 * @author Mattia
 */
public class ArrayDictionary implements Dictionary{
    
    /*
        Esempio Dizionario:
            
        - Listino Prezzi(codice, prezzo)
        - Traduttore...
        
    */
   
    //realizziamo un array non ordianto
    private Pair[] p;
    private int pSize;
    
    public ArrayDictionary(){
        p = new Pair[1]; // qualsiasi dimensione != 0
        makeEmpty();
    }
    
    public boolean isEmpty(){
        return (pSize == 0);
    }
    
    public void makeEmpty(){
        pSize = 0;
    }
    
    public void remove(Comparable key){
        for(int i=0;i<pSize;i++){
            
            if(p[i].getKey().equals(key)){ //p[i].getKey().compareTo(key)
                p[i] = p[--pSize]; // pSize--; p[i] = p[pSize];
                return;
            }
        }
        throw new DictionaryItemNotFound();
        
    }
    public Object find(Comparable key){
        for(int i=0;i<pSize;i++){
            
            if(p[i].getKey().equals(key)){ //p[i].getKey().compareTo(key)
                p[i] = p[--pSize]; // pSize--; p[i] = p[pSize];
                return p[i].getValue();
            }
        }
        throw new DictionaryItemNotFound();
    }
        
    public void insert(Comparable key,Object value){
        if(key==null){
            throw new IllegalArgumentException();
        }    
        
        try{
            remove(key); //trucco!
        }
        catch(DictionaryItemNotFound e){
        
        } 
        
        if(pSize == p.length){
            p = resize(p,2*pSize);
        }
        p[pSize++]= new Pair(key,value);
        
        
    }    
    
    private Pair[] resize(Pair[] oldArray, int newLength){
        assert newLength >= pSize;
        Pair[] temp = new Pair[newLength];
        for (int i = 0; i < pSize; i++) {
            temp[i] = oldArray[i];
        }
        return temp;
    }    
    
    private class Pair{ //interna al dizionario
        private Comparable key;
        private Object value;
        
        public Pair(Comparable k, Object v){
            setKey(k);
            setValue(v);
        }
        
        public Comparable getKey(){
            return key;
        }
        public Object getValue(){
            return value;
        }
        
        public void setKey(Comparable k){
            key = k;
        }
        
        public void setValue(Object v){
            value = v;
        }
            
    }
    
    private class DictionaryItemNotFound extends RuntimeException{
        
    }
    
    
}
