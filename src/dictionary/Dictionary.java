/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dictionary;



/**
 *
 * @author Mattia
 */
public interface Dictionary extends Container{
    
    /*
        Un dizionario è un ADT con le seguenti proprietà:
        - è un contenitore per dati non in sequenza
        - consente l'inserimento di coppie di dati di tipo CHIAVE/VALORE, con la chiave che deve essere ordinabile
          (Comparable) ed unica nell'insieme dei dati memorizzati
            - non possono esistere nel dizionario due valori con identica chiave
        - consente di effettuare in modo efficiente la ricerca e rimozione di valori usando la chiave come
          identificatore
    
        L'analogiaa con il dizionario di uso comune è molto forte:
            - le chiavi sono le singole parole
            - il valore corrispondente ad una chiave è la definizione della parola nel dizionario
            - tutte le chiavi sono distinte
            - ad ogni chiave è associato uno ed un solo valore
            - la ricerca di un valore avviene tramite la sua chiave
    
    
        Un dizionario può esser realizzato con una array:
        - ogni cella dell'array contiene un riferimento ad una coppia chiave/valore
            - un oggetto di tipo Pair (da definire)
        - Ci sono due strategie possibili:
            - mantente le chiavi ordinate nell'array
            - mantenere le chiavi non ordinate nell'array
            - si usa un array riempito solo in parte
    
    
    
         DIZIONARIO in un ARRAY ORDINATO
         
         Se le n chiavi vengono conservate ordinate nell'array:
         - la ricerca ha prestazioni O(log n)
            - si può usare la ricerca per bisezione
         - l'inserimento ha prestazioni O(n)
            - si usa l'ordinamento per inserimento in un array ordinato
            - con diverso inserimento, occorre ordinare l'intero array, con prestazioni almeno O(n log n)
         - la rimozione ha prestazioni O(n)
            - bisogna fare una ricerca, e poi spostare mediamente n/2 elementi per mantenere l'ordinamento
            O(log n)di ricerca + O(n) spostamento per la compattazione dell'array = O(n)
        
    
         DIZIONARIO in un ARRAY NON ORDINATO
         
         Se le n chiavi dell'array sono non ordinate:
         - la ricerca ha prestazioni O(n)
            - bisogna usare la ricerca lineare
         - l'inserimento ha rpestzioni O(n)
            - bisogna prima fare una ricerca, poi è sufficiente cercare se c'la coppia e s ec'è sovrascriverla,
              o nel caso contrario inserire il nuovo elemento nell'ultima posizione dell'array, perché 
              l'ordinamento non interessa.
         - la rimozione ha prestazioni O(n)
            - bisogna prima fare una ricerca, e poi spostare nella posizone trovata l'ultimo elemento dell'array,
              perché l'ordinamento non interessa
    */

    
    /* l'inserimento va sempre a buon fine: 
        - se la chiave non esiste, la coppia key/value viene aggiunta al dizionario; 
        - se la chiave esiste già, il valore ad essa associato viene sovrascritto con il nuovo valore;
        - se key è null viene lanciata IllegalArgumentException
        
    */
    void insert(Comparable key, Object value);
    
    /*
    la rimozione della chiave rimuove anche il corrispondente valore dal dizionario e lancia DictionaryItemNotFound
    se la chiave non esite
        
    */
    void remove(Comparable key);
    
    /*
    la ricerca per chiave resistuisce soltanto il valore ad essa associato nel dizionario e lancia 
    DictionaryItemNotFound se la chiave non esiste
    
    */
    Object find(Comparable key);
}
