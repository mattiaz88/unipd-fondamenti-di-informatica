/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package unipd;

import ADT_ReverseStack.ReverseStack;
import ADT_CheckBrackets.CheckBrackets;
import ADT_Sort.*;
import ADT_Calculator.*;
import stack.*;
import queue.*;
import recursion.*;
import sort.*;
import dictionary.*;

import Bank.*;

/**
 *
 * @author mattia
 */
public class UniPd {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //new Permutation();
        //ReverseStack.main(new String[0]);
        //CheckBrackets obj= new CheckBrackets();
        //new Stack_MergeSort();
        //new Queue_MergeSort();
        //new RPN_Calculator();
        new RPN_Calculator();
    }
    
}
