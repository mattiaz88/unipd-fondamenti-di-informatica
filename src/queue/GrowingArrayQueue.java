/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package queue;

/**
 *
 * @author Mattia
 */
public class GrowingArrayQueue extends FixedArrayQueue {

  
    public void enqueue(Object obj){
        if(back == v.length){
            v = resize(v,2*back);
        }
        super.enqueue(obj);
    };

    
    // resize the underlying array holding the elements
    private Object[] resize(Object[] v,int capacity) {
        assert capacity >= back;
        Object[] temp = new Object[capacity];
        for (int i = 0; i < back; i++) {
            temp[i] = v[i];
        }
        return temp;
    }
    
}
