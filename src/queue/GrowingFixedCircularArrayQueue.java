/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package queue;


/**
 *
 * @author Mattia
 */
public class GrowingFixedCircularArrayQueue extends FixedCircularArrayQueue {

    ///////////////////////////////////
       
    
    public void enqueue(Object obj){
        // controlla la cella libera alla dx di back 
         if(increment(back) == front){
            
            //PRESTAZIONI O(1)
            v = resize(v,2*v.length);
            //se si ridimensiona l'array e la zona utile
            //della coda si trova attorno alla sia fine,
            // la seconda metà del nuovo array rimane vuota
            // e provoca un malfunzionamento della coda,
            // che si risolve spostandovi la parte della
            // coda che si trova all'inizio dell'array
            if(back<front){
                System.arraycopy(v,0,v,v.length/2,back);
                back+= v.length/2;
            }
        }
        
        super.enqueue(obj);
    };

    
     // resize the underlying array holding the elements
    private Object[] resize(Object[] v,int capacity) {
        assert capacity >= v.length;
        Object[] temp = new Object[capacity];
        for (int i = 0; i < v.length; i++) {
            temp[i] = v[i];
        }
        return temp;
    }
}
