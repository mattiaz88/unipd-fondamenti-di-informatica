/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package queue;


/**
 *
 * @author Mattia
 */
public class FixedCircularArrayQueue extends FixedArrayQueue {

    //front e back si inseguono sempre ma non si devono superare
    ///////////////////////////////////
       
    protected int increment(int index){
        
        if(index + 1 < v.length){
            return index + 1;
        }
        else{
            return 0;
        }
    }
    
    public void enqueue(Object obj){
        // controlla la cella libera alla dx di back 
         if(increment(back) == front){
            throw new FullQueueException();
        }
        v[back]=obj;
        back = increment(back);
    };

    
    public Object dequeue(){
        Object obj = getFront();
        //non andrebbe bene perché la cella alla sx di back dovrebbe sempre essere vuota per motivi tecnici: contrllare isEmpty()...
        front = increment(front);
        return obj; 
    }
    
    
}
