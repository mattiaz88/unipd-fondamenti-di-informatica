/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_StringArrayStack;


import stack.*;

public class StringArrayStack extends GrowingArrayStack implements Stack{ 

    /*  REALIZZAZIONE DI UNA PILA SICURA
    
        Tutte le strutture dati possono contenere oggetti di qualsiasi tipo.
        Solitamente vengono utiilizzate per contenere collezioni omogenee di oggetti, ma in generale questo
        vincolo non c'è.
        Anche quando vengono usate come contenitori omogenei, è possible introdurvi per errore oggetti
        disomogenei. 
       
        Per realizzare una struttura dati che possa contenere soltanto oggetti di un certo tipo, per esempio 
        una pila che contenga soltanto stringhe, si può riscrivere tutto il codice modificando solo il metodo push
    
        Si potrebbe pensare di evitare il controllo di tipo nel metodo modificando il tipo del parametro:
    
        public void push(String obj){ ... }
        
        NON FUNZIONA: il compilatore segnala che la classe non definisce tutti i metodi richiesti dall'
        interfaccia Stack -> il metodo push ha una firma diversa
    
        
        ALTRA TENTAZIONE:
    
        public void push(Object obj){ hrow new InvalidTypeInStackException(); }
        public void push(String obj){ ... }
    
        - FUNZIONA se un esemplare di questa classe viene usato mediante una variabile di tipo riferimento alla 
        classe 
        - NON FUNZIONA se invece viene usato mediante un riferimento di tipo Stack !
    
    */
    public void push(Object obj){
        if(!(obj instanceof String)){ //se ok punta ad un oggetto di tipo String
            throw new InvalidTypeInStackException();
        }
        // Un modo più elegante sfrutta l'ereditarietà, che è utile per evitare di ricopiare codice
        // Il metodo  push viene sovrascritto (avendo firma identica), per cui negli esemplari di 
        // StringArrayStack verrà invocato il metodo qui definito e non quello di GrowingArrayStack
        super.push(obj);
        
        //if(vSize == v.length){
        //    v = resize(v,2*vSize);
        //}
        //v[vSize++]=obj;
    };
    
    private class InvalidTypeInStackException extends RuntimeException{
    
    }
    
    /////////////////////////////////////////
    
    
}
