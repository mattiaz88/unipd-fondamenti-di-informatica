/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_StringArrayStack;

import stack.*;


/**
 *
 * @author Mattia
 */
public class Main{
    
  
   public static void main() {
       
       /*
           Usando StringArrayStack è possible fare il cast  esplicitpo dopo l'estrazione di un elemento con la
           certezza che andrà a buon fine. 
           Viene la tentazione di  ridefinire il metodo pop come segue, per non dover fare il cast 
       
           public String pop(){ ... }
       
           Questo NON FUNZIONA! il compilatore seganala che la classe non definisce tutti i metodi richiesti 
           dall'interfaccia Stack: il metodo pop ha una firma diversa
       
           Alternativa senza usare l'ereditarietà:
           - usiamo una pila come variabile privata di esemplare
           - invece di usare l'erediarietà, si usa l'incapsulamento
       
           
           public class StringArrayStack extends GrowingArrayStack implements Stack{ 
            
                private Stack s = new GrowingArrayStack();
                public void makeEmpty(){
                    s.makeEmpty();
                }
                public boolean isEmpty(){
                    return s.isEmpty();
                }
                public void push(Object obj){
                    s.push(obj);
                };
               
                ...
           }

       */
       
       Stack s = new StringArrayStack();
       s.push("pippo");
       String st = (String) s.pop();
       s.push(new Integer(2)); //lancia un'eccezione
   }
   
}
