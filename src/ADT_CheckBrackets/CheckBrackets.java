/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_CheckBrackets;
import stack.*;
/**
 *
 * @author mattia
 */
public class CheckBrackets {
    
    private String expression;
    private boolean x;
    
    
    public CheckBrackets(){
        String expression = "a + [c + (g + h) + (f + zx) ]";
        this.setExpression(expression);
        this.x = this.checkBrackets();
        
    }
    
    public CheckBrackets(String expression){
        this.setExpression(expression);
        this.x = this.checkBrackets();
    }
    
    public void setExpression(String expression){
        this.expression=expression;
    }
    
    
    public boolean checkBrackets(){
        
        Stack stack = new GrowingArrayStack();
        
        for(int i=0;i<expression.length();i++){
            char c = expression.charAt(i);
            if(isBracket(c)){
                if(isOpenBracket(c)){
                    //trasformo un char in Character Obj
                    stack.push(c);
                }
                else{
                    //lo stack può essere vuoto
                    try{
                        /*
                            Sappiamo che serve il CAST perché l'operazione di assegnamento è potenzialmente
                            pericolosa: verrebbe lanciata l'eccezione ClassCastException, la cui gestione
                            non è obbligatoria, ma può esser comunque gestita
                        */
                        //Object obj = stack.pop();
                        //Character ch = (Character) obj;
                        try{
                            Character ch = (Character) stack.pop();
                            
                            char cc = ch.charValue();
                            if(!areMatching(c,cc)){
                                return false;
                            }
                        }
                        catch(ClassCastException e){
                            //gestione dell'errore
                        }
                        /*
                            In alternativa si può usare l'operatore instanceof
                        
                            Object obj = stack.pop();
                            if(obj instanceof Character){
                                Character ch = (Character) obj;
                                ..
                            }
                            else{
                                //gestione dell'errore
                            }
                        */
                        
                    }
                    catch(EmptyStackException e){
                        return false;
                    }
                }
            }
        }
        
        return true;
    }
    
    private boolean isBracket(char c){
        if(isOpenBracket(c) || isCloseBracket(c)){
            return true;
        }
        return false;
    }
    
    private boolean isOpenBracket(char c){
        if(c == '{' || c == '[' || c == '(' ){
            return true;
        }
        return false;
    }
    private boolean isCloseBracket(char c){
        if(c == '}' || c == ']' || c == ')'){
            return true;
        }
        return false;
    }
    private boolean areMatching(char c, char cc){
        if(c == '}' && cc == '{'){
            return true;
        }
        else if(c == ']' && cc == '['){
            return true;
        }
        else if(c == ')' && cc == '('){
            return true;
        }
        return false;
    }
        
}
