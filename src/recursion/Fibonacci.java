/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package recursion;


/**
 *
 * @author Mattia
 */
public class Fibonacci {
    
    private static final int number = 6 ;
    private static final String method = "recursive" ;

    public Fibonacci(){
        try{
            int result = 0;
            switch(method){
                case "recursive":result = fibonacciRecursive(number);break;
                default:break;
            }
                
            System.out.println(result);
        }
        catch(IllegalArgumentException e){
            System.out.println("number must be greater or equals to 0");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    /*
        Fib(n) = { n, 0<=n<2 
                   Fib(n-2) + Fib(n-1), n>=2
                }
    
        Ricorsione multipla -> ricorsione non in coda
        Il metodo invoca se stesso più volte durante una sua esecuzione
        
        Eliminare una ricordione di questo tipo è facile, però si può dimostrare che è sempre possibile:
        il proccessore esegue le istruzioni in sequenza e non ne ouò tenere altre in attesa, quindi
        l'interprete deve farsi carico di eliminare la ricorsione usando il RuntimeStack
    
        Deve essere usata con molta attenzione perché potrebbe portare a prorammi molto inefficienti
    
        Si noti che il tempo di elaborazione cresce molto rapidamente.
      
    */
    private int fibonacciRecursive(int number){
        
        if(number<0){
            throw new IllegalArgumentException();
        }
        else if(number < 2){
            return number;
        }
        else{
            
            return (fibonacciRecursive(number-2) + fibonacciRecursive(number-1));
        }
    }
    

    
} 
