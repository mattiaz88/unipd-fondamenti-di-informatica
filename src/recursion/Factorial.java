/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package recursion;


/**
 *
 * @author Mattia
 */
public class Factorial {
    
    private static int number = 5 ;
    private static final String method = "recursive" ;
    private int result;

    public Factorial(int number){
        Factorial.number = number;
        result=run();
    }
    
    public Factorial(){
        System.out.println(run());
    }
    
    
    public int getValue(){
        return result;
    }
    
    private int run(){
        try{
            int result;
            switch(method){
                case "recursive":result = factorialRecursive(number);break;
                default:result = factorial(number);break;
            }
                
            //System.out.println(result);
            return result;
        }
        catch(IllegalArgumentException e){
            System.out.println("number must be greater or equals to 0");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
        return 0;
    }
    
    /*
        0!=1
        1!=1
        2!=2(1)=2
        3!=3(2(1))=6
        4!=4(3(2(1)))=24
        5!=5(4(3(2(1))))=120
   
    
        Ricorsione in coda
        
        Codice meglio leggibile, 
        ma meno efficiente perché il sistema deve gestire delle invocazioni sospese
    */
    public int factorialRecursive(int number){
        
        if(number<0){
            throw new IllegalArgumentException();
        }
        else if(number == 0){
            return 1;
        }
        else if(number > 0){
            int p = number;
            p = p * factorialRecursive(p-1);
            return p;
        }
        return 0;
    }
    
    /*
        La ricorsione in coda può essere agevolmente eliminata,  
        scrivendo codice non ricorsivo equivalente, 
        trasformando il metodo ricorsivo in un metodo che usa un ciclo
    */
    public int factorial(int number){
        
        if(number<0){
            throw new IllegalArgumentException();
        }
        else if(number == 0){
            return 1;
        }
        else if(number > 0){
            int p = number;
            for(int i=number-1;i>0;i--){
                p = p * i;
            }
            return p;
        }
        return 0;
    }
    

    
} 
