/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package recursion;
import java.util.Arrays;

/**
 *
 * @author Mattia
 */
public class Permutation {
    
    private static final String stringa = "ABCDEFG" ;

    public Permutation(){
        try{
            String[] result = permutationRecursive(stringa);
            
            Factorial factorial = new Factorial(stringa.length());
            System.out.println("Total Permutation calculated = "+ String.valueOf(factorial.getValue()) );
            System.out.println("Total Permutation returned = "+ result.length);
            System.out.println(Arrays.toString(result));
        }
        catch(IllegalArgumentException e){
            System.out.println("string must be setted");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    /*
        permutazioni = (n-1)! permutazioni possibili
    */
    private String[] permutationRecursive(String testo){
        
        //gestiamo i casi base
            
        /*
            Quando un metodo deve restituire un oggetto, nei casi in cui riceve un parametro non corretto
            di solito restitusce null
        */
        /*
            Valutiamo che anche l'ordine in cui vengono valutate le due condizioni è molto importante.
            Se p è null, la prima condizione è vera per la strategia del cortocircuito nella valutazione 
            dell'operatore || la seconda condizione non viene valutata: se venisse valutata, verrebbe lanciata 
            l'eccezione NullPointerException !!
        */
        if(testo==null || testo.length()==0){
            /*
                In questo caso il metodo deve restituire un oggetto di tipo un po' particolare, in quanto è 
                un array. Sarebbe comunque corretto restituire null, ma di solito di preferisce restituire un
                array di lunghezza 0, in modo che il metodo invocante riceva un array valido, seppure vuoto.
                In queso modo, il codice funzionerebbe, mentre non funzionarebbe se utilizzassimo null.
            
                String[] x = permutations("");
            */
            return new String[0];
        }
        else if(testo.length()==1){
            return new String[]{testo};
        }
        else{
            //isoliamo il primo carattere
            String first = testo.substring(0,1);
            //passo ricorsivo: permutazioni incomplete
            String[] strings = permutationRecursive(testo.substring(1));
            //numero di permutazioni da generare
            String[] returnStrings = new String[strings.length * testo.length()];
            
            for(int j=0;j<strings.length;j++){
                String s = strings[j];
                for(int i=0;i<testo.length();i++){
                    String sLeft = s.substring(0,i);
                    String sRight = s.substring(i);
                    returnStrings[(i*strings.length) + j] = sLeft +first + sRight;
                }
            }
            
            
            return returnStrings;
        }
        
    }
    
    

    
} 
