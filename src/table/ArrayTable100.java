/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package table;



/**
 *
 * @author Mattia
 */
public class ArrayTable100 implements Table{
    
    /*
        La tabella è un dizionario con prestazioni ottime: tutte le operazioni sono O(1); 
        ma con le seguenti limitazioni:
        - le chiavi devono essere numeri interi (non negativi) 
        - l'intervallo di variabilità delle chiavi deve essere noto a priori: per dimensionare la tabella
        - se il fattore di riempimento è molto basso, si ha un grande spreco di memoria
          - ciò avviene se le chiavi sono molto disperse nel loro insieme di variabilità
    
   */
    
    
    private Object[] v;
    private int count; //count rende isEmpty O(1)
    
    
    public ArrayTable100(){
        makeEmpty();
    }
    
    public void makeEmpty(){
        count = 0;
        v = new Object[100];
    }
    
    public boolean isEmpty(){
        return (count == 0);
    }
    
    private void check(int key){
        if(key < 0 || key >= v.length){
            throw new InvalidPositionTableException();
        }
    }
    
    // O(1)
    public void insert(int key,Object value){
        check(key);
        if(v[key]==null){
            v[key]=value;
            count++;
        }
        else{
            v[key]=value;
        }
    }
    
    // O(1)
    public void remove(int key){
        check(key);
        if(v[key]!=null){
            v[key] = null;
            count--;
        }
    }
    
    // O(1)
    public Object find(int key){
        check(key);
        return v[key];
    }
    
    private class InvalidPositionTableException extends RuntimeException{
        
    }
    
    
    
}
