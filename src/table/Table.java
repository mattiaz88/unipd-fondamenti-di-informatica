/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package table;





/**
 *
 * @author Mattia
 */
public interface Table extends Container{
    
    /*
       TABELLA = DIZIONARIO in cui le chiavi sono numeri interi
    
       Se imponiamo una restrizione al campo di applicazione di un dizionario (supponiamo che le chiavi siano
       numeri interi appartenenti ad un intervall noto a priori) allora si può realizzare molto semplicemente un
       dizionario  con prestazioni O(1) per tutte le operazioni.
    
       Si usa un array che contiene soltanto i riferimenti ai valori, usando le chiavi come indici nell'array:
       - le celle dell'array che hanno come indice una chiave che non appartiene al dizionario hanno il valore null
    
       -> Un dizionario con chiavi numeriche intere viene detto tabella o tavola (table). 
          L'analogia è la seguente:
          - un valore è una riga nella tabella
          - le righe sono numerate usando le chiavi
          - alcune righe possono essere vuote
       
       Il tipo di dati astratto Table ha lo stesso comportamento del dizionario: l'unica differenza è che le chiavi
       non sono riferimenti ad oggetti di tipo Comparable, ma sono numeri interi(che evidentemente non sono 
       confrontabili) 
    
        
       La tabella potrebbe avere una dimensione variabile, cioè utilizzare un array di dimensione crescente quando
       sia necessario.
       - l'operazione di inserimento richiede, però, un tempo O(n) ogni volta che è necessario un ridimensionamento
       - le prestazioni nel peggiore dei casi sono O(n), perché il ridimensionamento può avvenire anche tutte le 
         volte
    
      La tabella non utilizza la memoria in modo efficiente:
      - l'occupazione di memoria richiesta per contenere n dati non dipenden da n in modo lineare
         - come avviene invece per tutti gli altri ADT ma dipende dal contenuto informativo presente nei dati,
           in particolare dal valore della chiave massima
    
      Può essere necessario un array di milioni di elementi per contenere poche decine di dati:
        - Si definisce fattore di riempimento (load factore) della tabella il numero di dati contenuti nella tabella
          diviso per la dimensione della tabella stessa
    
     
         
    */
    void insert(int key, Object value);
    void remove(int key);
    Object find(int key);
}
