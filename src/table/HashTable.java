/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package table;



/**
 *
 * @author Mattia
 */
public interface HashTable extends Container{
    
    /*
        HASH
        
        Se vogliamo eliminare il problema dell'aver già noto a priori l'intervallo di variabilità delle chiavi,
        dobbiamo fissare la dimensione della tabella in modo arbitrario, e di conseguenza l'intervallo di
        variabilità delle chiavi utilizzabili. 
        Per usare  chiavi esterne all'intervallo, si usa ina funzione di trasformazione delle chiavi: 
        FUNZIONE DI HASH
        
        Una funzione di hash ha
        - come dominio l'insieme delle chiavi che indentificano univocamente i dati da inserire nel dinzionario
        - come codominio l'insieme degli indici validi per accedere ad elementi della tabella
          -> il risultato dell'applicazione èdella funzione di hash ad una chiave si chiama chiave ridotta
    
        Se manteniamo la limitazione di usare numeri interi come chiavi unsa semplice funzione di hash è il 
        calcolo del resto della divisione intera tra la chiave e la dimensione della tabella.
        
        Per come è definita, la funzione di hash non è invertibile: chiavi diverse possono avere lo stesso valore
        per la funzione di hash.
    
        
        COLLISIONI
        
        Il fatto che la funzione di hash non sia univoca genera un problema nuovo: inserendo un valore nella
        tabella può darsi che la sua chiave ridotta sia uguale a quella di un valore già presente nella tabella
        e avente una diversa chiave, ma la stessa chiave ridotta.
        Questo fenomeno si chiama COLLISIONE nella tabella: una tabella che usa chiavi ridotte si chiama HASH TABLE
    
        
        TABELLA HASH CON BUCKET
        E' un sistema di risoluzione delle collisioni e risolve il problema delle collisioni usando una lista associata ad ogni cella dell'array:
        - l'array è un array di riferimenti a liste
        - ciascuna contiene le coppie chiave/valore che hanno la stessa chiave ridotta
    
        Un bucket è una delle liste associate ad una chiave ridotta
    
        Le prestazioni delle tabella hash con bucket non sono più di O(1) per tutte le operazioni: 
        - esse dipendono fortemente dalle caratteristiche della funzione di hash
            - caso peggiore: la funzione di hash restituisce sempre la stessa chiave ridotta, per ogni chiave possibile
            - caso intermedio: se tutti i dati vengono in un'unica lista, le prestazioni della tabella hash degenerano
                               in quelle di una lista -> tutte le operazioni sono O(n)
            - caso migliore: la funzione di hash resitutisce chiavi ridotte che si distribuiscono uniformemente nella
                             tabella -> tutte le operazioni sono O(n/M) dove M è la dimensione della tabella
                             -> per avere prestazioni O(1) occorre dimensionare la tabella in modo che M sia dello
                                stesso ordine di grandezza di n.
    
        RIASSUMENDO
        In una tabella hash con bucket si ottengono orestazioni ottimali (tempo-costanti) se:
        - la dimensione della tabella è circa uguale al numero di dati che saranno memorizzati nella tabella
            - fattore di rimepimento circa unitario, così si riduce al minimo anche lo spreco di memoria
        - la funzione di hash genera chiavi ridotte uniformemente distribuite:
            - liste di lunghezza quasi uguale alla lunghezza media
            - le liste hanno quasi tutte lunghezza 1 !
    
            
        -> Per oggetti generici la classe Object mette a disposizione il metodo hashCode che resitutisce un int con
           buon proprietà di distribuzione uniforme: viene calcolata na chiave ridotta a partire dall'indirizzo dell'
           oggetto  in memoria.
        
        
    */
    
    void insert(Object key, Object value);
    void remove(Object key);
    Object find(Object key);
}
