/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_ReverseStack;


import stack.*;

public class ReverseStack { //UN ESEMPIO

   public static void main (String[] args){
       Stack s = new StackX();
       s.push("Pippo");
       s.push("Pluto");
       s.push("Paperino");
       printAndClear(s);
       System.out.println();
       s.push("Pippo");
       s.push("Pluto");
       s.push("Paperino");
       printAndClear(reverseAndClear(s));
       
   }
   
   private static Stack reverseAndClear(Stack s){
       Stack p = new StackY();
       while(!s.isEmpty()){
           p.push(s.pop());
       }
       return p;
       
   }
   
   private static void printAndClear(Stack s){
       while(!s.isEmpty()){
           System.out.println(s.pop());//invocherà toString(Object o)
       }
   }
    
    
}
