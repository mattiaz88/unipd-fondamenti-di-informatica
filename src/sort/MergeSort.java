/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sort;
import java.util.Arrays;

/**
 *
 * @author Mattia
 */
public class MergeSort {
    
   private static int[] array = {5,7,9,1,8};
    /*
     *  MergeSort
     *   
     *  prestazioni migliori rispetto a quelle dell'ordinamento per selezione
     *  T(n) = numero di accessi necessari per ordinare un array di n elementi
     *  T(n) = 2T(n/2) + 5n -> n + 5n*log2 n -> n log2 n 
     *  Nelle notazioni "O grande" non si indica la base dei logaritmi, perché il logaritmo in una base
     *  si può trasformare nel logaritmo in un'altra base con un fattore moltiplicativo, che va ignorato
     *  Quindi le prestazioni dell'ordinamento MergeSort hanno un andamento O( n log n) e quindi sono
     *  migliori di O(n*n)
    */
    
    /*  
        Esempio 5 7 9 1 8
        
        1 - dividere l'array in 2 parti uguali circa
        2 - ordinare ciascuna delle 2 parti separatamente attraverso MergeSort
        3 - unire le 2 parti ordinate (fusione o merge) 
        
        Ora è facile costrutire l'array ordinato, prendendo sepre il primo elemento da uno dei sotto-array,
        scegliendo il più piccolo.
    
        C'è una situazione in cui le 2 parti sono sicuramente odinate: quando contengono un solo elemento.
    
    */
    public MergeSort(){
        
       
        try{
            mergeSort();
            System.out.println(Arrays.toString(array));
        }
        catch(IllegalArgumentException e){
            System.out.println("number must be greater or equals to 0");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void mergeSort(){
        
        mergeSort(array);
        
    }
    
    private void mergeSort(int[] data){
       
       //caso base
       if (data.length < 2){
           return;
       }
       
       //dividiamo l'array in 2 parti circa uguali
       int mid = (1 + data.length)/2;
       int[] left = new int[mid];
       int[] right = new int[data.length-mid];
       System.arraycopy(data, 0, left, 0, mid); // esecuzione 1n
       System.arraycopy(data, mid, right, 0, data.length-mid); // esecuzione 1n
       
       //passi ricorsivi per problemi più semplici
       //ordiniamo entrambe le 2 parti separatamente attraverso la tecnica della ricorsione multipla (doppia)
       mergeSort(left); //  esecuzione T(n/2)
       mergeSort(right); // esecuzione T(n/2)
       
       //fusione 
       merge(data,left,right); // esecuzione 3n
        
    }
    
    /* fusione con ordinamento ( esecuzione 3n ):
       n accessi per scrivere n elementi dell'array ordinato + 2n accessi -> ogni elemento da scrivere 
       richiede la lettura di 2 elementi, uno da ciascuno dei 2 array da fondere
    */ 
    private void merge(int[] data,int[] left,int[] right){
        int indexData = 0, indexLeft = 0, indexRight = 0;
        
        while(indexLeft<left.length && indexRight<right.length ){
            if(left[indexLeft]<right[indexRight]){
                //prima utilizza l'indice e poi lo incrementa
                data[indexData++]=left[indexLeft++];
            }
            else{
               data[indexData++]=right[indexRight++]; 
            }
        }
        
        //infine quando finisco l'ordinamento accodo il contenuto dell'array non ho scorso
        // sicuramente uno dei 2 array non viene scorso
        while(indexLeft<left.length){
            data[indexData++]=left[indexLeft++];
        }
        while(indexRight<right.length){
            data[indexData++]=right[indexRight++];
        }
    }
    

    
} 
