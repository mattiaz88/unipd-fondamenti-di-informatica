/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sort;
import java.util.Arrays;

/**
 *
 * @author Mattia
 */
public class SelectionSort {
    
   private static int[] array = {11,9,17,5,12};
    /*
     *  SelectionSort
     *   
     *  Per valutare l'efficienza di un algoritmo si misura il tempo in cui viene eseguito si insiemi di
     *  dati di dimensioni via via maggiori
     *  Il tempo di esecuzione di un algortimo dipende dal numero e dal tipo di istruzioni in linguaggio
     *  macchina che vengono eseguite dal processore. Per fare un'analisi teorica facciamo un conteggio del
     *  numero di accessi in lettura e scrittura ad un elemnto dell'array.
     *  
     *  T(n) = 1/2n*n + 9/2*n - 5 -> facendo una semplificazione, tenendo presente che ci interessano
     *  le prestazioni per valori elevati di n (andamento asintotico) 
     *  T(n) = 1/2*n*n == n*n -> O(n*n) 
     *  T(n) = O(n) = O(n log n)
     *  
     *  Si dice quindi che per ordinare un array con l'agoritmo per selezione si effettua un numero di accessi 
     *  che è dell'ordine di n*n. 
     *  Per esprimere sinteticamente questo concetto si usa la notazione "O grande" e si dice che il numero di 
     *  accessi è di O(n*n), ignorando quindi coefficienti e costanti.
     *  
     * 
    */
    
    /*  
        Scorrendo l'array di lunghezza n, si fa scorrere l'indice i da 1 a n-1 ripetendo i seguenti passi:
        1 - si ricerca l'elemento minore della sottosequenza A[i..n] dell'array
        2 - si scambia l'emento con l'elemetno i-esimo
   
        Mancano tutti i controlli di pre-condizioni
         - a[]==null
         - 0<from<a.length
    */
    public SelectionSort(){
        
       
        try{
            selectionSort();
            System.out.println(Arrays.toString(array));
        }
        catch(IllegalArgumentException e){
            System.out.println("number must be greater or equals to 0");
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void selectionSort(){
        
        selectionSort(array);
        
    }
    
    private void selectionSort(int[] data){
       
        // per ogni ciclo ci vogliono (n + 4)  accessi
       for(int index=0;index<data.length;index++){
           int minPos = findMinPos(data,index);  
           if(minPos!=index)swap(data,minPos,index);
       }
        
    }
    
   
    //per trovare l'elemento minore si fanno n accessi
    private int findMinPos(int[] data,int from){
        
        int pos = from;
        for(int i=from+1;i<data.length;i++){
            if(data[i]<data[pos]){
                pos=i;
            }
        }
        
        return pos;
    }
    
    // per scambiare 2 elementi si fanno 4 accessi
    private void swap(int[] data,int indexTo,int indexFrom){
        
       int temp = data[indexTo];
       data[indexTo]=data[indexFrom];
       data[indexFrom]=temp;
    }
    
    
    
} 
