/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Set;

import linkedList.*;

/**
 *
 * @author Mattia
 */
public class ArraySortedSet implements SortedSet {

    private Comparable[] v = new Comparable[100];
    private int vSize = 0;
    
    public void makeEmpty(){
        vSize = 0;
    }
    public boolean isEmpty(){
        return (vSize==0);
    }
    public void add(Object obj){
        throw new IllegalArgumentException();
    }
    
    public void add(Comparable obj){
        if(contains(obj)){
            return;
        }
        
        if(vSize == v.length){
            resize(v,2*vSize);
        }
        v[vSize++]=obj;
    }
    
    public boolean contains(Object obj){
        
        for(int i=0;i<vSize;i++){
           if(v[i].equals(obj)){
               return true;
           } 
        }
        return false;
    }
    public Object[] toArray(){
        
        return toSortedArray();
        //non deve essere disordinato per forza

    }
    public Comparable[] toSortedArray(){ // O(n)
        
        Comparable[] x = new Comparable[vSize];
        System.arraycopy(v, 0, x, 0, vSize);
        return x;

    }
    
    
    // resize the underlying array holding the elements
    private Object[] resize(Object[] v,int capacity) {
        assert capacity >= vSize;
        Object[] temp = new Object[capacity];
        for (int i = 0; i < vSize; i++) {
            temp[i] = v[i];
        }
        return temp;
    }
    
    
    //OPERAZIONI SU INSIEMI
    //UNIONE con MergeSort
    // Prestazioni O(n log n) anzichè quadratiche
    public static Set union(SortedSet s1,SortedSet s2){
        Set x = new ArraySortedSet();
        Comparable[] leftS1 = s1.toSortedArray();
        Comparable[] rightS2 = s2.toSortedArray();
        
        int i=0;
        int j=0;
        while(i<leftS1.length && j<rightS2.length){
            if(leftS1[i].compareTo(rightS2[j])<0){
                x.add(leftS1[i]);
                i++;
            }
            else if(leftS1[i].compareTo(rightS2[j])>0){
                x.add(rightS2[j]);
                j++;
            }
            else { //sono uguali
                x.add(leftS1[i]);
                i++;
                j++;
            }
            
        }
        
       
        while(i<leftS1.length){
           x.add(leftS1[i]);
        }
        while(j<rightS2.length){
           x.add(leftS1[j]);
        }
        
        return x;
    }
    
    //INTERSEZIONE con MergeSort
    // Prestazioni O(n log n) anzichè quadratiche
    public static Set intersect(SortedSet s1,SortedSet s2){
        Set x = new ArraySortedSet();
        Comparable[] leftS1 = s1.toSortedArray();
        Comparable[] rightS2 = s2.toSortedArray();
        
        int j=0;
        for (int i = 0; i < leftS1.length; i++) {
            while(j<rightS2.length){
                if(leftS1[i].compareTo(rightS2[j])>0){
                    j++;
                }    
                else if(leftS1[i].compareTo(rightS2[j])==0){
                    x.add(leftS1[i]);
                }
            
            }
            if(j==rightS2.length){
                break;
            }
        }
        
        return x;
    }
    
    //SOTTRAZIONE con MergeSort
    // Prestazioni O(n log n) anzichè quadratiche
    public static Set substract(SortedSet s1,SortedSet s2){
        Set x = new ArraySortedSet();
        Comparable[] leftS1 = s1.toSortedArray();
        Comparable[] rightS2 = s2.toSortedArray();
        
        int i;
        int j=0;
        for (i = 0; i < leftS1.length; i++) {
            while(j<rightS2.length){
                if(leftS1[i].compareTo(rightS2[j])>0){
                    j++;
                }    
                else if(leftS1[i].compareTo(rightS2[j])!=0){
                    x.add(leftS1[i]);
                }
            
            }
            if(j==rightS2.length){
                break;
            }
        }
        
        while(i<leftS1.length){
            x.add(leftS1[i]);
        }
        
        return x;
    }
    
}
