/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Set;

import linkedList.*;

/**
 *
 * @author Mattia
 */
public interface SortedSet extends Set {

    void add(Comparable obj);
    Comparable[] toSortedArray();
    
}
