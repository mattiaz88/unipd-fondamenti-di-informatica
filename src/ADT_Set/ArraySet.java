/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Set;

import linkedList.*;

/**
 *
 * @author Mattia
 */
public class ArraySet implements Set {

    private Object[] v = new Object[100];
    private int vSize = 0;
    
    public void makeEmpty(){
        vSize = 0;
    }
    public boolean isEmpty(){
        return (vSize==0);
    }
    public void add(Object obj){
        if(contains(obj)){
            return;
        }
        
        if(vSize == v.length){
            resize(v,2*vSize);
        }
        v[vSize++]=obj;
    }
    public boolean contains(Object obj){
        
        for(int i=0;i<vSize;i++){
           if(v[i].equals(obj)){
               return true;
           } 
        }
        return false;
    }
    public Object[] toArray(){
        
        Object[] x = new Object[vSize];
        System.arraycopy(v, 0, x, 0, vSize);
        return x;

    }
    
    
    // resize the underlying array holding the elements
    private Object[] resize(Object[] v,int capacity) {
        assert capacity >= vSize;
        Object[] temp = new Object[capacity];
        for (int i = 0; i < vSize; i++) {
            temp[i] = v[i];
        }
        return temp;
    }
    
    //OPERAZIONI SU INSIEMI
    //UNIONE 
    // Se contains è O(n), e quindi anche add(), questa operazione è O(n*n)
    public static Set union(Set s1,Set s2){
        Set x = new ArraySet();
        // inseriamo gli elementi del primo insieme
        Object[] tempS1 = s1.toArray();
        Object[] tempS2 = s2.toArray();
        for(int i=0;i<tempS1.length;i++){
           x.add(tempS1[i]);
        }
        // inseriamo tutti gli elementi del secondo insieme, sfruttando le proprietà di add (niente 
        // duplicati)
        for(int i=0;i<tempS2.length;i++){
           x.add(tempS2[i]);
        }
        
        return x;
    }
    
    //INTERESEZIONE TRA INSIEMI
    public static Set intersection(Set s1,Set s2){
        Set x = new ArraySet();
        Object[] tempS1 = s1.toArray();
        for(int i=0;i<tempS1.length;i++){
           if(s2.contains(tempS1[i])){
               x.add(tempS1[i]);
           }
        }
        
        return x;
    }
    
    //SOTTRAZIONI TRA INSIEMI
    public static Set substract(Set s1,Set s2){
        Set x = new ArraySet();
        Object[] tempS1 = s1.toArray();
        for(int i=0;i<tempS1.length;i++){
           if(!s2.contains(tempS1[i])){
               x.add(tempS1[i]);
           }
        }
        return x;
    }
}
