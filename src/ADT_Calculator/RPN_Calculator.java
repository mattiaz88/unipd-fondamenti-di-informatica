/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ADT_Calculator;
import ADT_Sort.*;
import stack.*;
import java.util.StringTokenizer;
/**
 *
 * @author mattia
 */
public class RPN_Calculator {
    /*
        Nonostante l'apparente maggiore complessità, le prestazioni asintotiche rimangono O(n log n)
    
        Tutte le operazioni sulle pile sono O(1) come gli accessi ad un elemento di un array, l'analisi
        delle prestazioni dell'algoritmo fornisce ancora 
        
        T(n) = 2T(n/2) + O(n) -> O(n log n)
    */
    
    
   /*
        Vogliamo risolvere il problema di calcolare il risultato di un'espressione aritmetica (ricevuta come
        String) contenente somme, sottrazioni, moltiplicazioni e divisioni.
    
        Utilizziamo il calcolo di un'espressione aritmetica che utilizza la notazione postfissa perché 
        utilizzare una notazione infissa sarebbe piuttosto complesso. Tale notazione prende il nome di
        NOTAZIONE POLACCA INVERSA (RPN, Reverse Polish Notation)
    
        7 1 2 + 4 * 5 6 + - /  =>  7/[(1-2)*4-(5*6)]
        
        - In tale notazione non sono ammesse parentesi, nè sarebbero necessarie
        - I due operandi di ciascun operatore si trovano alla sua sinistra
    */
    
    private Stack stack;
    private String rpnString;
    
    public RPN_Calculator(){

       rpnString = "7 1 2 + 4 * 5 6 + - /";
       double result = evaluateRPN(rpnString);
       System.out.println(result); //result must be 7 => 7 / (12-11)
    }
    
    
    private double evaluateRPN(String s){
       
        Stack stack = new GrowingArrayStack();
        StringTokenizer tokenizer = new StringTokenizer(s);
        
        while(tokenizer.hasMoreTokens()){
            String x = tokenizer.nextToken();
            //System.out.println(x.toString());
            //controllo se è un operando
            try{
                Double.parseDouble(x);
                stack.push(x);
            }
            //altrimenti controllo se è un operatore
            catch(NumberFormatException e){
                double res = evalOperator(x,(String)stack.pop(),(String)stack.pop());
                //System.out.println(res);
                stack.push(Double.toString(res));
            }
        }
        
        double result = Double.parseDouble((String)stack.pop());
        
        if(!stack.isEmpty()){
            throw new RuntimeException();
        }
        
        return result;
    }
    
    private double evalOperator(String operator,String right,String left){
        
        Double opLeft = Double.parseDouble(left);
        Double opRight = Double.parseDouble(right);
        Double result = 0.0;
        
        if(operator.equals("+")){
            result = opLeft+opRight;
        }
        else if(operator.equals("-")){
            result = opLeft-opRight;
        }
        else if(operator.equals("*")){
            result = opLeft*opRight;
        }
        else if(operator.equals("/")){
            result = opLeft/opRight;
        }
        else{
            throw new RuntimeException();
        }
    
        return result;
        
    }
        
    
 
}
